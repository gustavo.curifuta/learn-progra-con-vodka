
# Tarea wea wea wea (0?)
La descarga y subida de estas tareas cuenta como tarea 0

## Instrucciones

### 1. Crear una cuenta en gitlab
Ir a https://gitlab.com/users/sign_up y crear una cuenta, si tienes correo google o twitter abajo tiene la opción para usarlos como cuenta también en caso de que te de paja rellenar el formulario jaja

### 2. Descarga
Para descargar este archivo se debe descargar mediante **git**

Ejecutar en el CMD o en la consola abierta desde VScode (que basicamente es el mismo cmd): 

```
git clone https://gitlab.com/gustavo.curifuta/learn-progra-con-vodka.git
```
(Si, git ademas de permitirte guardar tus cambios y volver al pasado también te permite subir tus cambios y combinarlos con los de alguien mas, en este comando estas bajando el repositorio mio con mis cambios)

nota 1: es probable que te pida un correo y una contraseña, ahi usas los de tu cuenta gitlab creada en el paso 1
nota 2: si abres la consola desde el VScode aconsejo hacerlo desde una carpeta vacia

### 3. Subir la respuesta
Para subir la respuesta a esta tarea (o cualquier avance) se deben ejecutar los siguientes comandos :
```
git add .
```
(trackea las modificaciones que hiciste)
```
git commit -m "<descripción de que wea contienen los cambios>"
```
(crea un commit, un commit es una especie de checkpoint, una especie de "save game" en donde guardas el ultimo estado del repositorio)
```
git push origin master
```
(sube tus cambios)
Es probable que te pida un correo y una contraseña, ahi usas los de tu cuenta gitlab creada en el paso 1

### Guia de comandos git
|Comando  |Descripción  |
|--|--|
| git status | Te muestra el estado de tus cambios, que archivos se modificaron, eliminaron, etc |
| git log | Te muestra el historial de cambios |
| git add <nombre_archivo> | Agrega los cambios del archivo al seguimiento, si en vez de poner el nombre del archivo se pone un punto ("git add .") agrega automáticamente todos los archivos |
| git commit -m <mensaje> | crea un commit con los cambios agregados con git add (checkpoint) |
| git push origin master | sube tus cambios |
| git pull origin master | te actualiza con los cambios o actualizaciones que hallan subido otros |
| git checkout <commit_hash> | te regresa a algún commit anterior especifico (ES SOLO PARA MIRAR, SI SE QUIERE REGRESAR DE FORMA PERMANENTE SE DEBE USAR git reset --hard)|
| git checkout master | te regresa al commit mas reciente |
| git reset --hard <commit_hash> | Te resetea de forma permanente al commit que tu le indiques |
